<?php


namespace App;

use App\Entity\UserProfile;

class AppFutures{


    public function makeFakeProfile(){
        $faker = Faker\Factory::create();
        $profile = new UserProfile;
        $profile->setGivenName($faker->name);
        $profile->setFamilyName($faker->lastName);
        $profile->setBirthDate($faker->dateTimeBetween(-40,-20));
        return $profile;
    }

    public function addFakeProfileToDB(int $number){
        $profiles[] = array();
        for($i=0; $i<$number; $i++){
            $profile = makeFakeProfile();
            array_push($profiles, $profile );
        }

    }



}