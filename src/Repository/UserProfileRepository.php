<?php


namespace App\Repository;

use App\Entity\UserProfile;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class UserProfileRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserProfile::class);
    }

    public function dateFilterByAge(int $minAge, int $maxAge){
        $minDate = new DateTime;
        $minDate->format('Y-m-d H:i:s');
        $maxDate = new DateTime;
        $maxDate->format('Y-m-d H:i:s');
        $maxAge++;
        $minDate->modify('-'.$maxAge.' years');
        $maxDate->modify('-'.$minAge.' years');
        $em = $this->getEntityManager();
        $query = $em->createQuery('SELECT u FROM App\Entity\UserProfile u where u.birthDate BETWEEN ?1 AND ?2');
        $query->setParameter(1, $minDate);
        $query->setParameter(2, $maxDate);
        $profiles = $query->getResult();
        return $profiles;
    }

}