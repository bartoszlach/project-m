<?php

namespace App\Tests;

use App\Services\MyService;
use PHPUnit\Framework\TestCase;

class MyServiceTest extends TestCase
{
//    protected $myService;
//    public function __construct(MyService $myService)
//    {
//        $this->$myService = $myService;
//    }

    public function testSummary()
    {
        $myService = new MyService();
        $result = $myService->summary(2,6);
        $this->assertEquals(8, $result);

    }
}
