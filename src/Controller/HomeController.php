<?php

namespace App\Controller;

use App\Dto\AgeDto;
use App\Entity\UserProfile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\UserProfileRepository;
//use Fzaninotto\Faker;

class HomeController extends AbstractController
{
    private $userProfileRepository;
    public function __construct(UserProfileRepository $userProfileRepository)
    {
        $this->userProfileRepository = $userProfileRepository;
    }

    /**
     * @Route("/home", name="home")
     */
    public function index()
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);

    }

    /**
     * @Route("/date", name="date")
     * @param Request
     */
    public function dateFilter(Request $request){

        $age = new AgeDto();
        $form = $this->createFormBuilder($age)
            ->add('minAge', NumberType::class)
            ->add('maxAge', NumberType::class)
            ->add('save', SubmitType::class, ['label' => 'Search'])
            ->getForm();
        $profiles = new UserProfile();
        $form->handleRequest($request);
        if ($form->isSubmitted() ) {
            /**@var AgeDto $age */
            $age = $form->getData();
            $profiles = $this->userProfileRepository->dateFilterByAge($age->getMinAge(), $age->getMaxAge());
            dump($profiles);
        }

        return $this->render('home/index.html.twig', [
            'form' => $form->createView(),
            'profiles' => $profiles,
        ]);
    }

//    public function makeFakeProfile(){
//        require_once 'vendor/fzaninotto/faker/src/autoload.php';
//        $faker = Faker\Factory::create();
//        $profile = new UserProfile;
//        $profile->setGivenName($faker->name);
//        $profile->setFamilyName($faker->lastName);
//        $profile->setBirthDate($faker->dateTimeBetween(-40,-20));
//        return $profile;
//    }
//    /**
//     * @Route("/fakes", name="fakes")
//     */
//    public function generateFakes(int $number=50){
//
//        $profiles[] = array();
//        for($i=0; $i<$number; $i++){
//            $profile = $this->makeFakeProfile();
//            array_push($profiles, $profile );
//        }
//        $em = $this->getDoctrine()->getManager();
//        $em->persist($profiles);
//    }


}
