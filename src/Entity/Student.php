<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\StudentRepository")
 *
 */
class Student extends Person
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $indexNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $course;

    public function getIndexNumber(): ?string
    {
        return $this->indexNumber;
    }

    public function setIndexNumber(string $indexNumber): self
    {
        $this->indexNumber = $indexNumber;

        return $this;
    }

    public function getCourse(): ?string
    {
        return $this->course;
    }

    public function setCourse(string $course): self
    {
        $this->course = $course;

        return $this;
    }
}
